# The Oracle -- Crypto -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/crypto/the_oracle)

## Chal Info

Desc: `The oracle knows all, and it's kinda chatty. Is it telling you something?`

Flag: `TUCTF{D0nt_l3t_y0ur_s3rv3r_g1v3_f33db4ck}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/the_oracle)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/the_oracle:tuctf2019
```
